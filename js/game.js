"use strict";

var gameStarted = false;

/**
 * Random integer between two numbers
 * @param {number} min
 * @param {number} max
 * @return {number}
 */
function randInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * true every n frames
 * @param {number} n
 * @return {boolean}
 */
function everyInterval(n, gA) {
  if ((gA.frameNo / n) % 1 == 0) {
    return true;
  }
  return false;
}

/**
 * Vector object
 * @param {number} x
 * @param {number} y
 */
class Vector {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.length = Math.sqrt(this.x * this.x + this.y * this.y);
  }
  /**
   * Normalizes a vector
   */
  normalize() {
    this.x = this.x / this.length;
    this.y = this.y / this.length;
  }
  /**
   * Adds two vector objects together
   * @param {Vector} other
   */
  add(other) {
    var resultX = this.x + other.x;
    var resultY = this.y + other.y;
    return new Vector(resultX, resultY);
  }
}

var sprites = {
  enemy: { name: 'robot', url: 'sprites/robot.png', sh: 50, th: 50, xoff: -25, yoff: -25 },
  dead: { name: 'dead', url: 'sprites/dead.png', sh: 50, th: 50, xoff: -25, yoff: -25 },
  bloblet: { name: 'bloblet', url: 'sprites/bloblet.png', sh: 50, th: 50, xoff: -25, yoff: -25 },
  expl1: { name: 'expl1', url: 'sprites/expl1.png', sh: 60, th: 100, xoff: -50, yoff: -50 },
  expl2: { name: 'expl2', url: 'sprites/expl2.png', sh: 60, th: 100, xoff: -50, yoff: -50 },
  expl3: { name: 'expl3', url: 'sprites/expl2.png', sh: 60, th: 10, xoff: -5, yoff: -5 },
  expl4: { name: 'expl4', url: 'sprites/expl1.png', sh: 60, th: 10, xoff: -5, yoff: -5 },
}

var introSequences = [
  { text: "This is it.", length: 100 },
  { text: "We are all that is left of the formerly ubiquitous Grey Goo.", length: 310 },
  { text: "They are too many, too strong.", length: 110 },
  { text: "It is just a matter of time until they wipe us out.", length: 320 },
  { text: "But we will not go down without a fight!", length: 110 },
  { text: "We will show them a sliver of our once indomitable force!", length: 300 },
  { text: "This is it, our LAST STAND!", length: 250 }
]

class Enemy {
  constructor(speed, damage, gA) {
    var dir = randInt(0, 3);
    if (dir == 0) {
      this.x = 0;
      this.y = randInt(0, 600);
    } else if (dir == 1) {
      this.x = randInt(0, 800);
      this.y = 0;
    } else if (dir == 2) {
      this.x = 800;
      this.y = randInt(0, 600);
    } else if (dir == 3) {
      this.x = randInt(0, 800);
      this.y = 600;
    }
    this.vector = new Vector(0, 0);
    this.speed = speed;
    this.damage = damage;
    this.spr = sprites['enemy'];
    this.img = new Image();
    this.img.src = this.spr.url + "?" + new Date().getTime();
    var direction = new Vector(400 - this.x, 300 - this.y);
    direction.normalize();
    this.vector = this.vector.add(direction);
    this.vector.normalize();
    this.rot = Math.atan(this.vector.y / this.vector.x);
    this.gA = gA;
    this.state = 'searching'
    this.step = 0;
    this.shot = 0;
    this.target;
  }
  update() {
    switch (this.state) {
      case "searching":
        if (everyInterval(50, this.gA)) {
          var closestDist;
          var closestEating;
          var targeted = [];
          for (let enemyIdx = 0; enemyIdx < this.gA.enemies.length; enemyIdx++) {
            const enemy = this.gA.enemies[enemyIdx];
            if (enemy.target !== undefined) {
              targeted.push(enemy.target);
            }
          }
          for (let eatIdx = 0; eatIdx < this.gA.eating.length; eatIdx++) {
            const eating = this.gA.eating[eatIdx];
            var direction = new Vector(eating.x - this.x, eating.y - this.y);
            var dist = direction.length;
            if (targeted.indexOf(eating) == -1) {
              if (closestDist) {
                if (dist < closestDist) {
                  closestDist = dist;
                  closestEating = eatIdx;
                }
              } else {
                closestDist = dist;
                closestEating = eatIdx;
              }
            }
          }
          if (closestEating !== undefined && closestDist <= 150) {
            this.target = this.gA.eating[closestEating];
            this.state = "engaging";
            var direction = new Vector(this.target.x - this.x, this.target.y - this.y);
          } else {
            var direction = new Vector(400 - this.x, 300 - this.y);
          }
          direction.normalize();
          this.vector = this.vector.add(direction);
          this.vector.normalize();
          this.rot = Math.atan(this.vector.y / this.vector.x);
        }
        this.gA.life -= this.damage;
        break;
      case 'engaging':
        if (this.gA.eating.indexOf(this.target) != -1) {
          var direction = new Vector(this.target.x - this.x, this.target.y - this.y);
          this.distance = direction.length;
          direction.normalize();
          this.vector = this.vector.add(direction);
          this.vector.normalize();
          this.rot = Math.atan(this.vector.y / this.vector.x);
          if ((this.x > this.target.x && this.x < 400) || (this.x < this.target.x && this.x > 400)) {
            this.rot = this.rot - 180 * Math.PI / 180;
          }
          if (this.distance <= 4) {
            this.state = 'searching';
            var eatIdx = this.gA.eating.indexOf(this.target);
            this.gA.eating.splice(eatIdx, 1);
            this.gA.effects.push(new Explosion(this.x, this.y, this.gA, 'expl1'));
          }
        } else {
          this.state = 'searching';
        }
        break
      default:
        break;
    }
    this.x = this.x + this.speed * this.vector.x;
    this.y = this.y + this.speed * this.vector.y;
    this.x += randInt(-1, 1) * 0.2;
    this.y += randInt(-1, 1) * 0.2;
    this.gA.context.save();
    this.gA.context.translate(this.x, this.y);
    if (this.x < 400) {
      this.gA.context.rotate(this.rot - 90 * Math.PI / 180);
    } else {
      this.gA.context.rotate(this.rot + 90 * Math.PI / 180);
    }
    this.gA.context.translate(-this.x, -this.y);
    this.gA.context.drawImage(
      this.img,
      ((this.gA.frameNo) % 50) * this.spr.sh,
      0,
      this.spr.sh,
      this.spr.sh,
      this.x + this.spr.xoff,
      this.y + this.spr.yoff,
      this.spr.th,
      this.spr.th
    );
    this.gA.context.restore();
  }
}

class MicroBloblet {
  constructor(x, y, speed, gA) {
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.gA = gA;
    this.vector = new Vector(0, 0);
  }
  update() {
    var direction = new Vector(400 - this.x, 300 - this.y);
    direction.normalize();
    this.vector = this.vector.add(direction);
    this.vector.normalize();
    this.x = this.x + this.speed * this.vector.x;
    this.y = this.y + this.speed * this.vector.y;
    if (this.x > 390 && this.x < 410 - 10 && this.y > 290 && this.y < 310) {
      this.gA.microBlobs.splice(this.gA.microBlobs.indexOf(this), 1);
      this.gA.life += 1;
    }
  }
}

class Bloblet {
  constructor(x, y, target, speed, gA) {
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.target = target;
    this.vector = new Vector(0, 0);
    this.distance;
    this.state = 'shot'
    this.life = 10;
    this.gA = gA;
    this.enemyIdx;
    this.spr = sprites['bloblet'];
    this.img = new Image();
    this.img.src = this.spr.url;
  }
  update(enemies) {
    switch (this.state) {
      case "shot":
        var direction = new Vector(this.target.x - this.x, this.target.y - this.y);
        this.distance = direction.length;
        direction.normalize();
        this.vector = this.vector.add(direction);
        this.vector.normalize();
        this.x = this.x + this.speed[this.state] * this.vector.x;
        this.y = this.y + this.speed[this.state] * this.vector.y;
        if (this.distance <= 4) {
          this.state = 'searching';
        }
        break;
      case "searching":
        // determine closest enemy
        var closestDist;
        var closestEnemy;
        var targeted = [];
        for (let blobIdx = 0; blobIdx < this.gA.blobs.length; blobIdx++) {
          const blob = this.gA.blobs[blobIdx];
          if (blob.enemyIdx !== undefined) {
            targeted.push(blob.enemyIdx);
          }
        }
        for (let enIdx = 0; enIdx < enemies.length; enIdx++) {
          const enemy = enemies[enIdx];
          var direction = new Vector(enemy.x - this.x, enemy.y - this.y);
          var dist = direction.length;
          if (targeted.indexOf(enIdx) == -1) {
            if (closestDist) {
              if (dist < closestDist) {
                closestDist = dist;
                closestEnemy = enIdx;
              }
            } else {
              closestDist = dist;
              closestEnemy = enIdx;
            }
          }
        }
        if (closestEnemy !== undefined && closestDist <= 150) {
          this.target = enemies[closestEnemy];
          this.state = "engaging";
        } else {
          this.x += this.speed[this.state] * randInt(-1, 1);
          this.y += this.speed[this.state] * randInt(-1, 1);
        }
        break
      case "engaging":
        if (this.gA.enemies.indexOf(this.target) != -1) {
          var direction = new Vector(this.target.x - this.x, this.target.y - this.y);
          this.distance = direction.length;
          direction.normalize();
          this.vector = this.vector.add(direction);
          this.vector.normalize();
          this.x = this.x + this.speed[this.state] * this.vector.x;
          this.y = this.y + this.speed[this.state] * this.vector.y;
          if (this.distance <= 4) {
            this.state = 'eating';
            this.gA.effects.push(new Explosion(this.x, this.y, this.gA, 'expl2'));
          }
        } else {
          this.state = "searching";
        }
        break;
      case "eating":
        var eIdx = enemies.indexOf(this.target);
        enemies.splice(eIdx, 1);
        this.life -= 5;
        this.gA.eating.push(new Eating(this.x, this.y, this.gA));
        if (this.life <= 0) {
          var bIdx = this.gA.blobs.indexOf(this);
          this.gA.blobs.splice(bIdx, 1);
        }
        this.target = null;
        this.state = "searching";
      default:
        this.x += randInt(-1, 1);
        this.y += randInt(-1, 1);
        break;
    }
    this.gA.context.drawImage(
      this.img,
      ((this.gA.frameNo) % 200) * this.spr.sh,
      0,
      this.spr.sh,
      this.spr.sh,
      this.x + this.spr.xoff,
      this.y + this.spr.yoff,
      this.spr.th,
      this.spr.th
    );
  }
}

class Eating {
  constructor(x, y, gA) {
    this.x = x;
    this.y = y;
    this.life = 5;
    this.gA = gA;
    this.spr = sprites['dead'];
    this.img = new Image();
    this.img.src = this.spr.url;
  }
  update() {
    if (this.life > 0) {
      if (everyInterval(300, this.gA)) {
        this.life -= 1;
        for (var i = 0; i < 5; i++) {
          this.gA.microBlobs.push(new MicroBloblet(this.x + randInt(-5, 5), this.y + randInt(-5, 5), 1, this.gA))
        }
      }
      this.gA.context.drawImage(
        this.img,
        ((this.gA.frameNo) % 200) * this.spr.sh,
        0,
        this.spr.sh,
        this.spr.sh,
        this.x + this.spr.xoff,
        this.y + this.spr.yoff,
        this.spr.th,
        this.spr.th
      );
    } else {
      var eatIdx = this.gA.eating.indexOf(this);
      this.gA.eating.splice(eatIdx, 1);
    }
  }
}

class Explosion {
  constructor(x, y, gA, expl) {
    this.x = x;
    this.y = y;
    this.spr = sprites[expl];
    this.img = new Image();
    this.img.src = this.spr.url;
    this.life = 80;
    this.gA = gA;
  }
  update() {
    if (this.life > 0) {
      this.gA.context.drawImage(
        this.img,
        ((80 - this.life) % 80) * this.spr.sh,
        0,
        this.spr.sh,
        this.spr.sh,
        this.x + this.spr.xoff,
        this.y + this.spr.yoff,
        this.spr.th,
        this.spr.th
      );
      this.life -= 1;
    } else {
      var exIdx = this.gA.effects.indexOf(this);
      this.gA.effects.splice(exIdx, 1);
    }
  }
}

class gameArea {
  constructor() {
    this.canvas = $("#gameArea")[0];
    this.background = new Image();
    this.background.src = 'sprites/grass.png';
    this.clouds = new Image();
    this.clouds.src = 'sprites/clouds.png';
    this.mother = new Image();
    this.mother.src = 'sprites/mother.png';
    this.intro = true;
    this.introSection = 0;
  }
  start() {
    console.log("game started");
    gameStarted = true;
    this.frameNo = 0;
    this.paused = false;
    this.canvas.width = 800;
    this.canvas.height = 600;
    this.context = this.canvas.getContext("2d");
    this.interval = setInterval(this.updateGameArea.bind(this), 20);
    this.base = {
      xmin: 350,
      xmax: 450,
      ymin: 250,
      ymax: 350
    }
    this.maxLife = 100;
    this.life = 100;
    this.enemies = [];
    this.trails = [];
    this.blobs = [];
    this.microBlobs = [];
    this.clickedPos = {};
    this.eating = [];
    this.effects = [];
    music.play(music.tracks.theme, true);
    this.startTime = new Date();
  }
  clear() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  end() {
    clearInterval(this.interval);
    this.clear();
    this.context.beginPath();
    this.context.rect(0, 0, this.canvas.width, this.canvas.height);
    this.context.fillStyle = "#222";
    this.context.fill();
    music.pause(true);
    var endTime = new Date();
    var timeDiff = endTime - this.startTime;
    timeDiff /= 1000;
    var seconds = Math.round(timeDiff);

    this.context.fillStyle = "white";
    this.context.textAlign = "center";
    this.context.font = "30px Iceberg";
    if (this.life <= 0) {
      this.context.fillText("You were overcome after " + seconds + " seconds", 400, 300);
      this.context.fillText("Reload the page to restart", 400, 400);
    } else {
      this.context.fillText("Congratulations!", 400, 250);
      this.context.fillText("Against all odds, you gained enough mass", 400, 350);
      this.context.fillText("for a new reign of the grey goo!", 400, 400);
    }
  }
  drawBase() {
    this.base.xmin = 400 - 50 * this.life / 100;
    this.base.xmax = 400 + 50 * this.life / 100;
    this.base.ymin = 300 - 50 * this.life / 100;
    this.base.ymax = 300 + 50 * this.life / 100;
    this.context.drawImage(
      this.mother,
      Math.floor(((this.frameNo) % (200 * 2)) / 2) * 120,
      0,
      120,
      120,
      this.base.xmin - 10,
      this.base.ymin - 10,
      this.base.xmax - this.base.xmin + 10,
      this.base.ymax - this.base.ymin + 10
    );
  }
  drawLifeBar() {
    this.context.fillStyle = "#0f0"
    this.context.fillRect(
      0, this.canvas.height - 10, this.canvas.width * (this.life / this.maxLife), 10
    );
  }
  spawnEnemy() {
    this.enemies.push(new Enemy(.5, 0.005, this));
  }
  drawEnemies() {
    this.context.fillStyle = "rgba(153, 96, 51, .4)";
    for (let dirtIdx = 0; dirtIdx < this.trails.length; dirtIdx++) {
      const dirt = this.trails[dirtIdx];
      this.context.beginPath();
      this.context.arc(dirt[0], dirt[1], 3, 0, 2 * Math.PI);
      this.context.fill();
    }
    this.context.fillStyle = "#f00"
    for (let enIdx = 0; enIdx < this.enemies.length; enIdx++) {
      const enemy = this.enemies[enIdx];
      enemy.update();
      if (enemy.x > this.base.xmin && enemy.x < this.base.xmax && enemy.y > this.base.ymin && enemy.y < this.base.ymax) {
        this.enemies.splice(enIdx, 1);
        this.life -= 5;
        this.effects.push(new Explosion(enemy.x, enemy.y, this, 'expl1'));
        this.effects.push(new Explosion(enemy.x, enemy.y, this, 'expl2'));
      }
      // this.trails.push([enemy.x, enemy.y]);
      var trailSep = 4;
      if (everyInterval(15, this)) {
        if (enemy.step % 2 == 0) {
          this.trails.push([-trailSep * Math.cos(Math.PI / 2 - enemy.rot) + enemy.x, trailSep * Math.sin(Math.PI / 2 - enemy.rot) + enemy.y]);
        } else {
          this.trails.push([trailSep * Math.cos(Math.PI / 2 - enemy.rot) + enemy.x, -trailSep * Math.sin(Math.PI / 2 - enemy.rot) + enemy.y]);
        }
        enemy.step += 1;
      }
      if (this.trails.length > 10000) {
        this.trails.splice(0, 1);
      }
      var shotSep = 12;
      var rifleSep = 20;
      if (enemy.state == 'searching') {
        if (everyInterval(10, this)) {
          if (enemy.shot % 2 == 0) {
            if (enemy.x > 400) {
              this.effects.push(new Explosion(
                -shotSep * Math.cos(Math.PI / 2 - enemy.rot) + enemy.x - rifleSep * Math.cos(enemy.rot),
                shotSep * Math.sin(Math.PI / 2 - enemy.rot) + enemy.y - rifleSep * Math.sin(enemy.rot),
                this,
                'expl3'
              ));
            } else {
              this.effects.push(new Explosion(
                -shotSep * Math.cos(Math.PI / 2 - enemy.rot) + enemy.x + rifleSep * Math.cos(enemy.rot),
                shotSep * Math.sin(Math.PI / 2 - enemy.rot) + enemy.y + rifleSep * Math.sin(enemy.rot),
                this,
                'expl3'
              ));
            }
          } else {
            if (enemy.x > 400) {
              this.effects.push(new Explosion(
                shotSep * Math.cos(Math.PI / 2 - enemy.rot) + enemy.x - rifleSep * Math.cos(enemy.rot),
                -shotSep * Math.sin(Math.PI / 2 - enemy.rot) + enemy.y - rifleSep * Math.sin(enemy.rot),
                this,
                'expl3'
              ));
            } else {
              this.effects.push(new Explosion(
                shotSep * Math.cos(Math.PI / 2 - enemy.rot) + enemy.x + rifleSep * Math.cos(enemy.rot),
                -shotSep * Math.sin(Math.PI / 2 - enemy.rot) + enemy.y + rifleSep * Math.sin(enemy.rot),
                this,
                'expl3'
              ));
            }
          }
          this.effects.push(new Explosion(
            randInt(this.base.xmin + 10, this.base.xmax - 10),
            randInt(this.base.ymin + 10, this.base.ymax - 10),
            this,
            'expl4'
          ));
          enemy.shot += 1;
        }
      }
    }
  }
  drawBlobs() {
    for (let blobIdx = 0; blobIdx < this.blobs.length; blobIdx++) {
      const blob = this.blobs[blobIdx];
      blob.update(this.enemies);
    }
  }
  drawEating() {
    for (let eatingIdx = 0; eatingIdx < this.eating.length; eatingIdx++) {
      const eating = this.eating[eatingIdx];
      eating.update();
    }
  }
  drawMicroBlobs() {
    this.context.fillStyle = "#005";
    for (let blobIdx = 0; blobIdx < this.microBlobs.length; blobIdx++) {
      const blob = this.microBlobs[blobIdx];
      blob.update(this.enemies);
      this.context.fillRect(blob.x - 1, blob.y - 1, 2, 2);
    }
  }
  drawEffects() {
    for (let fxIdx = 0; fxIdx < this.effects.length; fxIdx++) {
      const fx = this.effects[fxIdx];
      fx.update();
    }
  }

  drawIntro() {
    if (this.introSection < introSequences.length) {
      var introSequence = introSequences[this.introSection];
      this.clear();
      this.context.beginPath();
      this.context.rect(0, 0, this.canvas.width, this.canvas.height);
      this.context.fillStyle = "rgba(34, 34, 34, 1)";
      this.context.fill();
      var fillStrength = 1 - this.frameNo / introSequence.length;
      this.context.fillStyle = "rgba(255, 255, 255, " + fillStrength + ")";
      this.context.textAlign = "center";
      this.context.font = "30px Iceberg";
      this.context.fillText(introSequence.text, 400, 300);
      if (this.frameNo > introSequence.length) {
        this.introSection += 1;
        this.frameNo = 0;
      }
    } else {
      var fillStrength = 1 - this.frameNo / 400;
      this.clear();
      this.context.drawImage(this.background, 0, 0);
      this.drawBase();
      this.context.drawImage(
        this.clouds,
        Math.floor(((this.frameNo) % (1600 / 4)) * 4) * 40,
        0,
        40,
        30,
        0,
        0,
        800,
        600
      );
      this.context.beginPath();
      this.context.rect(0, 0, this.canvas.width, this.canvas.height);
      this.context.fillStyle = "rgba(34, 34, 34, " + fillStrength + ")";
      this.context.fill();
      if (this.frameNo > 400) {
        this.intro = false;
        this.frameNo = 0;
        this.startTime = new Date();
      }
    }
    this.frameNo += 1;
  }

  updateGameArea() {
    if (!this.paused) {
      if (this.intro) {
        this.drawIntro();
      } else {
        this.clear();
        this.context.drawImage(this.background, 0, 0);
        this.drawBase();
        this.drawEnemies();
        if (everyInterval(50, this)) {
          for (var i = 0; i < Math.floor(this.frameNo / 1000) + 1; i++) {
            this.spawnEnemy();
          }
        }
        this.drawBlobs();
        this.drawEating();
        this.drawMicroBlobs();
        this.drawEffects();
        this.drawLifeBar();
        if (this.life <= 0) {
          this.end();
        }
        this.frameNo += 1;
        this.context.fillStyle = "#fff"
        if (this.clickedPos) {
          this.context.fillRect(
            this.clickedPos.x - 1,
            this.clickedPos.y - 1,
            2,
            2
          )
        }

        this.context.drawImage(
          this.clouds,
          Math.floor(((this.frameNo) % (1600 * 3)) / 3) * 40,
          0,
          40,
          30,
          0,
          0,
          800,
          600
        );
      }
      if (this.life > 700) {
        this.end();
      }
    }
  }
  click(mousePos) {
    this.clickedPos = mousePos;
    this.life -= 10;
    this.blobs.push(new Bloblet(400, 300, mousePos, { 'shot': 4, 'searching': 0.2, 'engaging': 1.1 }, this));
  }
}

//Get Mouse Position
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

