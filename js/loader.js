"use strict";

function preloadSprites(images, callback) {
    $("#loadSprites").fadeIn("fast");
    var loader = [];
    var sprites = Object.keys(images);
    for (var i = 0; i < sprites.length; i++) {
        loader.push(preload(images[sprites[i]]));
    }
    $.when.apply(null, loader).done(function () {
        callback(); // finished loading, proceed.
    });
}

function preload(img) {
    var deferred = $.Deferred();
    var imageObj = new Image();
    imageObj.src = img.url;
    imageObj.onload = function () {
        deferred.resolve();
    }
    return deferred.promise();
}